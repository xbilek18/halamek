svg4everybody();
(function jumbo() {

    slider = initSlider("#jumbo");

    if(slider){
        if (slider.allItemsArray.length > 1) {
            autoplay(7000, slider);
        }
    }

    function autoplay(interval, wallop) {
        var lastTime = 0;

        function frame(timestamp) {
            var update = timestamp - lastTime >= interval;
            if (update) {
                wallop.next();
                lastTime = timestamp;
            }
            requestAnimationFrame(frame);
        }
        requestAnimationFrame(frame);
    }

})();

(function projectsSlider() {
    var leftArrow = document.querySelector('#leftArrow');
    var rightArrow = document.querySelector('#rightArrow');

    var breakPoint = 801;

    slider = initSlider("#projectFullWrapper");

    if (slider) {
        window.addEventListener('resize', function(){
            handleSlide(slider.currentItemIndex);
        });

        slider.on('change', function(event){
            handleSlide(event.detail.currentItemIndex);
        });
    }

    function handleSlide(slideIndex){
        var slide = slideIndex;
        var isMobile = window.innerWidth < breakPoint;

        if (slide === 0){
            fill(rightArrow, "black");
            if(isMobile){
                fill(leftArrow, "black");
            } else {
                fill(leftArrow, "white");
            }
        }
        else {
            fill(rightArrow, "white");
            fill(leftArrow, "white");
        }
    }

    function fill(element, color){
        element.style.fill = color;
    }

})();

(function scrollTopTopArrow(){
    var hasScrollbar = window.innerWidth > document.documentElement.clientWidth;
    var arrow = document.querySelector('#arrowUp') || null;       

    if (arrow){
        clickListener(arrow, scrollToTop);
    }

    function scrollToTop() {
        var timeOut;
        if (document.body.scrollTop !== 0 || document.documentElement.scrollTop !== 0) {
            window.scrollBy(0, -30);
            timeOut = setTimeout(scrollToTop, 5);
        } else clearTimeout(timeOut);
    }

})();

(function menuToggle() {
    var burger = document.querySelector('#hamburgerMenu');
    var menu = document.querySelector('.menu');
    var header = document.querySelector('.header');
    var headerRight = document.querySelector('.header__right');

    var breakPoint = 801;
    var isMobile = window.innerWidth < breakPoint;

    if (isMobile) {
        window.addEventListener('click', menuClick);
    }

    window.addEventListener('resize', handleWindowResize);

    function handleWindowResize(){
        var isMobile =  window.innerWidth < breakPoint;   

        if(isMobile && getStyle(burger, "display") !== "none"){
            window.addEventListener('click', menuClick);
        } else {
            //reset mobile menu state
            window.removeEventListener('click', menuClick);

            menu.style = "";
            header.style = "";
            headerRight.style = "";
        }
    }

    function menuClick(event){
        clickedItem = event.target;
        isHamburgerClicked = hasClass(clickedItem, "svg-hamburger");
        isHidden = getStyle(menu, "opacity") !== "1";

        if(isHidden && isHamburgerClicked){
            menuAnimate("6.5rem", "flex", 1, "100%");
        } else {
            menuAnimate("", "none", 0, "46px");
        }
    }

    function menuAnimate(headerRightPadding, menuDisplay, menuOpacity, headerHeight){
        VelocityAnimate(headerRight, {paddingTop: headerRightPadding}, {duration: 100 });
        VelocityAnimate(menu, {opacity: menuOpacity}, {duration: 250, 
            complete: function() {  
                menu.style.display = menuDisplay; 
            }
        });
        VelocityAnimate(header, {height: headerHeight}, {duration: 350 });

    }

})();

(function viewSwitcher(slider){
    var setTiles = document.querySelector("#setTiles");
    var tilesWrapper = document.querySelector("#projectTilesWrapper");
    var fullWrapper = document.querySelector("#projectFullWrapper");
    var tiles = document.querySelector("#projectTiles");

    if(tilesWrapper && fullWrapper){
        setTiles.addEventListener('click', function(event){
            layoutToggle("tiles", tilesWrapper, fullWrapper, "screen-overlay--visible");
            goToListener(event);
        });


        tilesWrapper.addEventListener('click', function(){
            layoutToggle("full", tilesWrapper, fullWrapper, "screen-overlay--visible");
        });
    }

    function goToListener(event){
        if(hasClass(tilesWrapper, "screen-overlay--visible")){
            tiles.addEventListener('click', function(event){
                goTo(event);
            });
        }
    }

    function goTo(event){
        console.log('goto fired');
        clickedItemIndex = getElementIndex(event.target.parentElement, event.target.parentElement.parentElement);
        slider.goTo(clickedItemIndex);
        layoutToggle("full", tilesWrapper, fullWrapper, "screen-overlay--visible");
    }

    function layoutToggle(layout, tilesWrapper, fullWrapper, visible){
        if(layout === "tiles" && getStyle(tilesWrapper, "display") === "none"){
            fadeIn = tilesWrapper;

            addClass(fadeIn, visible);
            VelocityAnimate(fadeIn, {
                opacity: 1
            }, {
                duration: 180,
                easing: "ease-in",
            });

        } else if (layout === "full" && getStyle(tilesWrapper, "opacity") !== "0"){
            fadeOut = tilesWrapper;

            VelocityAnimate(fadeOut, {
                opacity: 0
            }, {
                duration: 180,
                easing: "ease-out",
                complete: function(){
                    removeClass(fadeOut, visible);
                }
            });

        } else {
            return;
        }
    }

})(window.slider);


function clickListener(element, executingFunction){
    eventListener(element, 'click', executingFunction);
}


function eventListener(element, ev, executingFunction){
    element.addEventListener(ev, function(event){
        executingFunction();
    });
}


function initSlider(selector){
    var element = document.querySelector(selector);
    var slider;

    if(element){
        slider = new Wallop(element);
    } else {
        slider = false;
    }
    
    return slider;
}


function VelocityAnimate(element, properties, setting, animate){
    animate = animate || Velocity;

    animate
        (   
            element, 
            properties, 
            setting
        );   
}

function initMap() {
    var styles = [{
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#444444"
        }]
    }, {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [{
            "color": "#c5c5c5"
        }]
    }, {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [{
            "visibility": "on"
        }, {
            "weight": "4.35"
        }, {
            "color": "#c5c5c5"
        }]
    }, {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "road",
        "elementType": "all",
        "stylers": [{
            "saturation": -100
        }, {
            "lightness": 45
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [{
            "visibility": "simplified"
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [{
            "visibility": "simplified"
        }, {
            "color": "#ffffff"
        }, {
            "lightness": "0"
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#ffffff"
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [{
            "color": "#ffffff"
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "labels.text",
        "stylers": [{
            "visibility": "on"
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "labels.icon",
        "stylers": [{
            "visibility": "on"
        }]
    }, {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [{
            "visibility": "on"
        }]
    }, {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#eeeeee"
        }, {
            "lightness": "62"
        }]
    }, {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [{
            "lightness": "75"
        }]
    }, {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [{
            "visibility": "on"
        }]
    }, {
        "featureType": "transit.line",
        "elementType": "all",
        "stylers": [{
            "visibility": "on"
        }]
    }, {
        "featureType": "transit.station.bus",
        "elementType": "all",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "transit.station.rail",
        "elementType": "all",
        "stylers": [{
            "visibility": "on"
        }]
    }, {
        "featureType": "transit.station.rail",
        "elementType": "labels.icon",
        "stylers": [{
            "hue": "#000000"
        }, {
            "saturation": "-100"
        }]
    }, {
        "featureType": "water",
        "elementType": "all",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#111111"
        }, {
            "lightness": "25"
        }, {
            "saturation": "-23"
        }]
    }, {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [{
            "saturation": "-11"
        }, {
            "lightness": "-61"
        }]
    }];
    if (document.getElementById('map')) {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 49.09688889,
                lng: 16.40238889
            },
            zoom: 14,
            disableDefaultUI: true,
            zoomControl: !('ontouchstart' in document.documentElement)
        });
        map.setOptions({
            styles: styles,
            scrollwheel: false
        });
        var marker = new google.maps.Marker({
            position: {
                lat: 49.08938889,
                lng: 16.40998889
            },
            icon: "./images/icons/marker.png",
            map: map,
            title: 'Halámek'
        });
        var contentString = '<div style="font-family: \'Rubik\';">Stříbský mlýn 1116/5<br>Ivančice<br> 664 91</div';
        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
    }
}

