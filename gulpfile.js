/*
You can use following commands:
    gulp
    gulp build: prepare static resources
    gulp watch : watch for changes in sass/html/js and reload browser on change
    gulp sprite: create sprite.png from images/icons
    gulp sass:lint : check sass file for errors and bad practices
*/

'use strict'

var gulp = require('gulp'),
    browserSync = require('browser-sync').create();

var umbraco_src = 'C:/WebSites/ibodies-umbraco/';

var plugin = require('gulp-load-plugins')({
    DEBUG: false,
    pattern: ['gulp-*', 'gulp.*','merge-stream','streamqueue'], 
    scope: ['dependencies', 'devDependencies'], 
    replaceString: /^gulp(-|\.)/, 
    camelize: true,
    lazy: true
});

gulp.task('default',['build'], function() {
    return plugin.mergeStream(
        gulp.src('./build/*.css')
        .pipe(gulp.dest(umbraco_src + 'css/')),
        gulp.src('./build/*.js')
        .pipe(gulp.dest(umbraco_src + 'scripts/'))
    );
});

gulp.task('build',['css-build','js-build'],function(){

});

gulp.task('watch', ['browser-sync','build'], function() {
    gulp.watch('sass/**/*.scss', ['css-build']);
    gulp.watch('script/**/*.js', ['js-reload']);
    gulp.watch('*.html').on('change', browserSync.reload);
});

gulp.task('browser-sync', ['sass', 'js'], function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('js-reload', ['js-build'], function(){
    browserSync.reload();
});

gulp.task('js', function() {
    return gulp.src('./script/script.js')
        .pipe(plugin.jshint())
        .pipe(plugin.jshint.reporter('default'));
});

gulp.task('js-concat', ['js'], function () {
    return plugin.streamqueue({ objectMode: true },
        gulp.src('./script/lazysizes.js'),
        gulp.src('./script/velocity.js'),
        gulp.src('./script/svg_ie.js'),
        gulp.src('./script/wallop.js'),
        gulp.src('./script/helpers.js'),
        gulp.src('./script/script.js'))
        .pipe(plugin.concat('./script.min.js'))
        .pipe(gulp.dest('build'));
});

gulp.task('js-build',['js-concat'], function (){
    var pump = require('pump');
    pump([
            gulp.src('build/*.js'),
            plugin.sourcemaps.init(),
            plugin.uglify(),
            plugin.sourcemaps.write('./'),
            gulp.dest('build')
        ]);
});

gulp.task('sass', ['sass:lint'], function () {
    return gulp.src('./sass/**/*.scss')
        .pipe(plugin.sass({ includePaths : ['./sass/'] }).on('error', plugin.sass.logError))
        .pipe(plugin.autoprefixer())
        .pipe(gulp.dest('./css'));
});

gulp.task('sass:lint', function () {
    return gulp.src(['./sass/**/*.s+(a|c)ss','!./sass/lib/**/*.s+(a|c)ss','!./sass/_png-sprite.scss'])
        .pipe(plugin.sassLint({
            options: {
                formatter: 'stylish'
            },
            configFile: './.sass-lint.yaml'
        }))
        .pipe(plugin.sassLint.format())
        .pipe(plugin.sassLint.failOnError());
});

gulp.task('css-build', ['sass'], function(){
    return gulp.src('./css/*.css')
        .pipe(plugin.concatCss('style.min.css'))
        .pipe(plugin.sourcemaps.init())
        .pipe(plugin.cleanCss())
        .pipe(plugin.sourcemaps.write('./'))
        .pipe(gulp.dest('./build'))
        .pipe(browserSync.stream());
});

gulp.task('sprite', ['png-sprite', 'svg-sprite'], function() {
        gulp.src('./symbol/svg/*')
        .pipe(gulp.dest('./images'))
});

gulp.task('png-sprite', function() {
    var spriteData = gulp.src('images/icons/*.png').pipe(plugin.spritesmith({
        imgName: 'images/sprite.png',
        cssName: 'sass/_png-sprite.scss',
        imgPath: '../images/sprite.png'
    }));
    return spriteData.pipe(gulp.dest('./'));
});

gulp.task('svg-sprite', function() {

    var config = {
        log: 'info',
        dest: './',
        mode: {
            symbol: {
                render: {
                    scss: {

                    }
                }
            }
        }
    };

    return gulp.src('./images/icons/*.svg', {
            cwd: './'
        })
        .pipe(plugin.plumber())
        .pipe(plugin.svgSprite(config)).on('error', function(error) {
            console.log(error);
        })
        .pipe(gulp.dest('./'));
});
gulp.task('copy:images',function(){
    return gulp.src('./images/**/*',{
        base: 'images'
    })
        .pipe(gulp.dest(umbraco_src + 'images'));
});